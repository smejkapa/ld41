﻿using Scripts;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenuController : MonoBehaviour
{
    public GameObject PauseMenuPanel;
    public GameObject QuitLevelButton;

    private bool _wasInputEnabled;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (PauseMenuPanel.activeInHierarchy)
                Resume();
            else
                Pause();
        }
    }

    public void Pause()
    {
        Time.timeScale = 0;
        _wasInputEnabled = GameManager.Instance.IsInputEnabled;
        GameManager.Instance.IsInputEnabled = false;
        PauseMenuPanel.SetActive(true);
    }

    public void Resume()
    {
        Time.timeScale = 1;
        GameManager.Instance.IsInputEnabled = _wasInputEnabled;
        PauseMenuPanel.SetActive(false);
    }

    public void QuitGame()
    {
        GameManager.Instance.QuitGame();
    }
}
