﻿using System.Collections.Concurrent;
using UnityEngine;

namespace Scripts.DataStructures
{
    public class ObjectPool
    {
        private readonly GameObject _objectPrefab;
        private readonly ConcurrentBag<GameObject> _objectPool = new ConcurrentBag<GameObject>();
        private readonly int _itemCountStep;
        private readonly Transform _baseTransform;

        private int _lastId;

        public ObjectPool(int itemCountStep, Transform baseTransform, GameObject objectPrefab)
        {
            _itemCountStep = itemCountStep;
            _baseTransform = baseTransform;
            _objectPrefab = objectPrefab;
            AddObjects();
        }

        private void AddObjects()
        {
            for (int i = 0; i < _itemCountStep; i++)
            {
                GameObject item = Object.Instantiate(_objectPrefab);
                item.transform.parent = _baseTransform;
                item.transform.position = Vector3.zero;
                item.transform.rotation = Quaternion.identity;
                item.GetComponent<IPoolable>().Reset();
                item.GetComponent<IPoolable>().Id = _lastId++;
                item.SetActive(false);
                _objectPool.Add(item);
            }
        }

        public GameObject GetObject()
        {
            GameObject item;
            if (_objectPool.TryTake(out item))
            {
                item.SetActive(true);
                //Debug.Log($"Giving item Id {item.GetComponent<IPoolable>().Id}");
                return item;
            }

            AddObjects();

            _objectPool.TryTake(out item);
            item.SetActive(true);
            return item;
        }

        public void PutObject(GameObject item)
        {
            item.transform.parent = _baseTransform;
            item.transform.position = Vector3.zero;
            item.transform.rotation = Quaternion.identity;
            item.GetComponent<IPoolable>().Reset();
            item.SetActive(false);
            //Debug.Log($"Returning item Id {item.GetComponent<IPoolable>().Id}");
            _objectPool.Add(item);
        }
    }
}
