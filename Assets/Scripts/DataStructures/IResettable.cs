﻿namespace Scripts.DataStructures
{
    public interface IResettable
    {
        void Reset();
    }
}
