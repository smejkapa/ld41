﻿namespace Scripts.DataStructures
{
    public interface IPoolable : IResettable
    {
        int Id { get; set; }
    }
}
