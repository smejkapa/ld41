﻿using System.Collections.Generic;
using Scripts.Girls;
using UnityEngine;

namespace Scripts
{
    public static class ModUtil
    {
        public delegate void CharacterModifier(GameObject character);

        public static readonly Dictionary<GirlType, List<CharacterModifier>> MODIFIERS =
            new Dictionary<GirlType, List<CharacterModifier>>
            {
                {
                    GirlType.Fire, new List<CharacterModifier>
                    {
                        (go) => { go.GetComponent<CharacterController>().Damage += 4; },
                        (go) => { go.GetComponent<CharacterController>().IsFireAuraActive = true; }
                    }
                },
                {
                    GirlType.Frost, new List<CharacterModifier>
                    {
                        (go) => { go.GetComponent<CharacterController>().Attack |= AttackModifier.Slow; },
                        (go) => { go.GetComponent<CharacterController>().Attack |= AttackModifier.Freeze; }
                    }
                },
                {
                    GirlType.Earth, new List<CharacterModifier>
                    {
                        (go) => { go.GetComponent<CharacterController>().LifeHandler.MaxHP += 20; },
                        (go) => { go.GetComponent<CharacterController>().IsHealOnClearActive = true; } 
                    }
                },
                {
                    GirlType.Air, new List<CharacterModifier>
                    {
                        (go) => { go.GetComponent<CharacterMovement>().Speed += 2; },
                        (go) => { go.GetComponent<CharacterController>().FireCooldown -= 0.2f; }
                    }
                }
            };
    }
}
