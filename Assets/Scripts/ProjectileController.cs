﻿using Scripts.DataStructures;
using Scripts.Enemy;
using UnityEngine;

namespace Scripts
{
    public enum ProjectileType
    {
        Fireball,
        BloodShot
    }

    public class ProjectileController : MonoBehaviour, IPoolable
    {
        public delegate void Destroyed(ProjectileController projectile);

        public ProjectileType Type;

        public float Damage { get; set; }
        public float Speed { get; set; }
        public Vector3 Direction { get; set; }
        public Destroyed OnDestroyed { get; set; }
        public string FriendlyTag { get; set; }
        public string EnemyTag { get; set; }
        public AttackModifier Attack { get; set; }

        public Color Color
        {
            set { _spriteRenderer.color = value; }
        }

        public int Id { get; set; }

        private SpriteRenderer _spriteRenderer;

        public void Awake()
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
        }

        public void Update()
        {
            transform.position += Direction * Speed * Time.deltaTime;
        }

        public void OnTriggerEnter2D(Collider2D collision)
        {
            if (!gameObject.activeSelf ||
                !collision.gameObject.activeSelf ||
                collision.CompareTag(FriendlyTag) || 
                collision.CompareTag("EnemyTouchTrigger"))
            {
                return;
            }

            if (collision.tag == EnemyTag)
            {
                collision.gameObject.GetComponent<LifeHandler>().TakeDamage(Damage);
                HandleAttackModifier(collision.gameObject);
            }
            OnDestroyed?.Invoke(this);
        }

        private void HandleAttackModifier(GameObject enemyGo)
        {
            var enemy = enemyGo.GetComponent<EnemyController>();
            if (enemy == null)
                return;

            if (Attack.HasFlag(AttackModifier.Freeze) && Random.value < 0.4)
            {
                enemy.Freeze();
            }
            else if (Attack.HasFlag(AttackModifier.Slow))
            {
                var enemyMovement = enemyGo.GetComponent<EnemyMovement>();
                if (enemyMovement != null)
                {
                    enemyMovement.SlowDown();
                }
            }
        }

        public void Reset()
        {
            Damage = 0.0f;
            Speed = 0.0f;
            Direction = Vector3.zero;
            OnDestroyed = null;
            FriendlyTag = string.Empty;
            EnemyTag = string.Empty;
            Attack = AttackModifier.None;
            Color = Color.white;
        }

        public static void DestroyFireball(ProjectileController projectile)
        {
            Pools.Instance.Fireballs.PutObject(projectile.gameObject);
        }

        public static void DestroyBloodShot(ProjectileController projectile)
        {
            Pools.Instance.BloodShots.PutObject(projectile.gameObject);
        }
    }
}
