﻿using UnityEngine;

namespace Scripts
{
    public class CharacterMovement : MonoBehaviour
    {
        public float Speed;
        private Rigidbody2D _rigidBody;
        private AudioSource _audioSource;

        private void Awake()
        {
            _rigidBody = GetComponent<Rigidbody2D>();
            Debug.Assert(_rigidBody != null);

            _audioSource = GetComponent<AudioSource>();
            Debug.Assert(_audioSource != null);
        }

        private void Update()
        {
            if (!GameManager.Instance.IsInputEnabled)
            {
                _rigidBody.velocity = Vector2.zero;
            }
            else
            {
                Vector2 velocity = _rigidBody.velocity;
                velocity.x = Speed * Input.GetAxis("Horizontal");
                velocity.y = Speed * Input.GetAxis("Vertical");

                _rigidBody.velocity = velocity;
            }

            // Play sound
            if (_rigidBody.velocity.sqrMagnitude > 0)
            {
                if (!_audioSource.isPlaying)
                    _audioSource.Play();
            }
            else
            {
                if (_audioSource.isPlaying)
                    _audioSource.Stop();
            }
        }
    }
}
