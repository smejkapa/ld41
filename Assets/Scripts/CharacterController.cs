﻿using System;
using System.Collections.Generic;
using Scripts.Enemy;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Scripts
{
    [Flags]
    public enum AttackModifier
    {
        None = 0,
        Slow = 1,
        Freeze = 2
    }

    public class CharacterController : MonoBehaviour
    {
        public float FireCooldown;
        public float ProjectileSpeed;
        public float Damage;
        public float EnemyTouchDamageTaken;
        public float InvulnerableTime;
        public float FireAuraModifierDamageTaken;
        public float FireAuraDamageGiven;
        public float OnClearHealAmount;
        public Text HpText;
        public Color FireballColor;
        public Color FrostbotColor;
        public Text HealText;
        public Sprite[] DirectionSprites;
        public AudioClip[] ShootSounds;
        public AudioClip[] DamageTakenSounds;
        public AudioClip[] HealSounds;

        public AttackModifier Attack { get; set; }
        public bool IsHealOnClearActive { get; set; }
        public bool IsFireAuraActive { get; set; }

        private float _currentFireCooldown;
        public LifeHandler LifeHandler { get; private set; }
        private float _currentInvulnerableTime;
        private SpriteRenderer _spriteRenderer;
        private AudioSource _characterAudioSource;
        private float _healFadeTime;
        private Rigidbody2D _rigidBody;

        public void Awake()
        {
            _rigidBody = GetComponent<Rigidbody2D>();
            Debug.Assert(_rigidBody != null);

            _characterAudioSource = gameObject.AddComponent<AudioSource>();
            _characterAudioSource.playOnAwake = false;

            LifeHandler = GetComponent<LifeHandler>();
            Debug.Assert(LifeHandler != null);
            LifeHandler.OnHpChange = OnHpChanged;
            LifeHandler.OnDeath = () => { GameManager.Instance.GameOver(); };

            _spriteRenderer = GetComponent<SpriteRenderer>();

            _currentInvulnerableTime = 0.0f;

            Util.IgnoreEnemyPlayerCollisions(true);
            HealText.gameObject.SetActive(false);
            //Attack = AttackModifier.Slow | AttackModifier.Freeze;
            //IsFireAuraActive = true;
            //IsHealOnClearActive = true;
        }

        public void Start()
        {
            if (IsFireAuraActive)
            {
                _spriteRenderer.color = new Color(1.0f, 0.5f, 0.25f, 1.0f);
            }
        }

        public void Update()
        {
            // Handle sprites
            if (_rigidBody.velocity.x > 0)
            {
                _spriteRenderer.sprite = DirectionSprites[3];
            }
            else if (_rigidBody.velocity.x < 0)
            {
                _spriteRenderer.sprite = DirectionSprites[2];
            }
            else if (_rigidBody.velocity.y > 0)
            {
                _spriteRenderer.sprite = DirectionSprites[0];
            }
            else if (_rigidBody.velocity.y < 0)
            {
                _spriteRenderer.sprite = DirectionSprites[1];
            }
            else
            {
                _spriteRenderer.sprite = DirectionSprites[1];
            }

            if (_currentFireCooldown > 0)
                _currentFireCooldown -= Time.deltaTime;
            else if (GameManager.Instance.IsInputEnabled)
                HandleFire();

            if (_currentInvulnerableTime > 0)
            {
                _currentInvulnerableTime -= Time.deltaTime;
            }
            else
            {
                LifeHandler.IsInvulnurable = false;
            }

            if (_healFadeTime > 0)
            {
                _healFadeTime -= Time.deltaTime;
                var startFade = 1.0f;
                if (_healFadeTime <= startFade)
                {
                    var alpha = Mathf.Lerp(0, 1, _healFadeTime / startFade);
                    HealText.color = new Color(HealText.color.r, HealText.color.g, HealText.color.b, alpha);
                }
            }
            else
            {
                HealText.gameObject.SetActive(false);
            }
        }

        private void HandleFire()
        {
            var fireDir = Vector3.zero;
            float projectileRotation = 0.0f;
            var fireH = Input.GetAxis("FireHorizontal");
            var fireV = Input.GetAxis("FireVertical");
            if (fireH > 0)
            {
                fireDir = Vector3.right;
                projectileRotation = -90.0f;
                _spriteRenderer.sprite = DirectionSprites[3];
            }
            else if (fireH < 0)
            {
                fireDir = Vector3.left;
                projectileRotation = 90.0f;
                _spriteRenderer.sprite = DirectionSprites[2];
            }
            else if (fireV > 0)
            {
                fireDir = Vector3.up;
                projectileRotation = 0.0f;
                _spriteRenderer.sprite = DirectionSprites[0];
            }
            else if (fireV < 0)
            {
                fireDir = Vector3.down;
                projectileRotation = 180.0f;
                _spriteRenderer.sprite = DirectionSprites[1];
            }

            if (fireDir != Vector3.zero)
            {
                _currentFireCooldown = FireCooldown;

                GameObject projectile = Pools.Instance.Fireballs.GetObject();
                var pc = projectile.GetComponent<ProjectileController>();
                pc.Direction = fireDir;
                pc.Damage = Damage;
                pc.transform.position = transform.position;
                pc.transform.Rotate(new Vector3(0, 0, projectileRotation));
                pc.Speed = ProjectileSpeed;
                pc.FriendlyTag = tag;
                pc.EnemyTag = "Enemy";
                pc.transform.parent = transform.parent;
                pc.OnDestroyed = ProjectileController.DestroyFireball;
                pc.Attack = Attack;
                if ((Attack & (AttackModifier.Slow | AttackModifier.Freeze)) != AttackModifier.None)
                {
                    pc.Color = FrostbotColor;
                }
                else
                {
                    pc.Color = FireballColor;
                }

                SoundUtil.PlayRandomSound(ShootSounds, _characterAudioSource);
                Debug.Log("--- Shoot");
            }
        }

        public void OnHpChanged(float oldHp, float newHp, bool isHeal)
        {
            ChangeHpText(newHp);
            if (!isHeal)
            {
                OnDamageTaken();
            }
            else
            {
                if (newHp > oldHp && oldHp > 0)
                {
                    HealText.text = $"+{newHp - oldHp} HP";
                    _healFadeTime = 3.0f;
                    HealText.gameObject.SetActive(true);
                    SoundUtil.PlayRandomSound(HealSounds, _characterAudioSource);
                }
            }
        }

        private void ChangeHpText(float newHp)
        {
            HpText.text = $"Health: {newHp}";
        }

        public void OnEnemyTouched(GameObject enemy)
        {
            var damage = EnemyTouchDamageTaken;
            if (IsFireAuraActive)
            {
                var controller = enemy.GetComponent<EnemyController>();
                controller.LifeHandler.TakeDamage(FireAuraDamageGiven);
                damage = EnemyTouchDamageTaken * FireAuraModifierDamageTaken;
            }
            LifeHandler.TakeDamage(damage);
        }

        private void OnDamageTaken()
        {
            if (!LifeHandler.IsInvulnurable)
            {
                LifeHandler.IsInvulnurable = true;
                _currentInvulnerableTime = InvulnerableTime;
                SoundUtil.PlayRandomSound(DamageTakenSounds, _characterAudioSource);
            }
        }

        public void OnRoomCleared()
        {
            if (IsHealOnClearActive)
            {
                LifeHandler.Heal(OnClearHealAmount);
            }
        }
    }
}
