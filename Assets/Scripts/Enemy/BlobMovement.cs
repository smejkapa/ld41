﻿using Scripts.DataStructures;
using UnityEngine;

namespace Scripts.Enemy
{
    public class BlobMovement : EnemyMovement
    {
        public AudioClip MoveSound;

        private AudioSource _moveAudio;

        public override void Awake()
        {
            base.Awake();
            _moveAudio = gameObject.AddComponent<AudioSource>();
            _moveAudio.playOnAwake = false;
            _moveAudio.loop = true;
            _moveAudio.clip = MoveSound;
        }

        public override void Update()
        {
            base.Update();
            if (_enemyController.IsFrozen)
            {
                return;
            }
            
            var player = GameManager.Instance.Player;
            Vector3 toPlayer = player.transform.position - transform.position;
            toPlayer.Normalize();
            _rigidbody.velocity = toPlayer * Speed * SpeedModifier;

            // Sounds
            if (_rigidbody.velocity.sqrMagnitude > 0 && GameManager.Instance.IsInputEnabled)
            {
                if (!_moveAudio.isPlaying)
                    _moveAudio.Play();
            }
            else
            {
                if (_moveAudio.isPlaying)
                    _moveAudio.Stop();
            }
        }

        public override void Reset()
        {
            base.Reset();
            _moveAudio?.Stop();
        }
    }
}
