﻿using Scripts.DataStructures;
using UnityEngine;

namespace Scripts.Enemy
{
    public class ShootAtPlayer : MonoBehaviour, IResettable
    {
        public float ShootCooldown;
        public float ProjectileSpeed;
        public float Damage;
        public AudioClip[] ShootSounds;

        private float _currentCooldown;
        private EnemyController _enemyController;
        private AudioSource _shootAudio;

        public void Awake()
        {
            _enemyController = GetComponent<EnemyController>();
            _shootAudio = gameObject.AddComponent<AudioSource>();
            _shootAudio.playOnAwake = false;
            _shootAudio.loop = false;
        }

        public void Update()
        {
            if (_enemyController.IsFrozen)
                return;

            if (_currentCooldown > 0)
            {
                _currentCooldown -= Time.deltaTime;
            }
            else
            {
                _currentCooldown = ShootCooldown;
                GameObject shot = Pools.Instance.BloodShots.GetObject();
                var pc = shot.GetComponent<ProjectileController>();
                Vector3 dirToPlayer = GameManager.Instance.Player.transform.position - transform.position;
                dirToPlayer.Normalize();
                pc.Damage = Damage;
                pc.Direction = dirToPlayer;
                pc.EnemyTag = "Player";
                pc.FriendlyTag = "Enemy";
                pc.OnDestroyed = ProjectileController.DestroyBloodShot;
                pc.Speed = ProjectileSpeed;
                pc.transform.parent = transform.parent;
                pc.transform.position = transform.position;
                SoundUtil.PlayRandomSound(ShootSounds, _shootAudio);
            }
        }

        public void Reset()
        {
            if (_shootAudio != null)
            {
                _shootAudio.Stop();
                _shootAudio.clip = null;
            }
            _currentCooldown = ShootCooldown;
            _currentCooldown += Random.Range(-0.2f * ShootCooldown, 0.4f * ShootCooldown);
        }
    }
}
