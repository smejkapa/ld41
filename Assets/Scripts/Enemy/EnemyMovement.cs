﻿using Scripts.DataStructures;
using UnityEngine;

namespace Scripts.Enemy
{
    public class EnemyMovement : MonoBehaviour, IResettable
    {
        public float Speed;

        protected Rigidbody2D _rigidbody;
        protected EnemyController _enemyController;

        public float SpeedModifier { get; set; }

        public float SlowTime { get; set; }

        public virtual void Awake()
        {
            _enemyController = GetComponent<EnemyController>();
            _rigidbody = GetComponent<Rigidbody2D>();
        }

        public virtual void Update()
        {
            if (_enemyController.IsFrozen)
            {
                _rigidbody.velocity = Vector2.zero;
                return;
            }

            if (SlowTime > 0)
            {
                SlowTime -= Time.deltaTime;
            }
            else if (!_enemyController.IsFrozen)
            {
                SpeedModifier = 1.0f;
            }
        }

        public void SlowDown()
        {
            SlowTime = 3.0f;
            SpeedModifier = 0.5f;
        }

        public virtual void Reset()
        {
            //Debug.Log("Movement reset");
            SpeedModifier = 1.0f;
            SlowTime = 0.0f;
        }
    }
}
