﻿using System;
using System.Collections;
using System.Collections.Generic;
using Scripts.DataStructures;
using Scripts.Map;
using UnityEngine;

namespace Scripts.Enemy
{
    public class EnemyController : MonoBehaviour, IPoolable
    {
        public EnemyType Type;

        public AudioClip[] HitSounds;

        private AudioSource _enemyAudioSource;

        private bool _isFrozen;

        public bool IsFrozen
        {
            get { return _isFrozen; }
            set
            {
                _isFrozen = value;
                //Debug.Log($"Set frozen to {value} for: {Id}");
            }
        }

        public LifeHandler LifeHandler { get; private set; }
        private SpriteRenderer _spriteRenderer;
        private EnemyMovement _enemyMovement;
        private float _frozenTime;
        private float _damageBlinkTime;

        public int Id { get; set; }

        public void Awake()
        {
            _enemyAudioSource = gameObject.AddComponent<AudioSource>();

            LifeHandler = GetComponent<LifeHandler>();
            Debug.Assert(LifeHandler != null);

            _spriteRenderer = GetComponent<SpriteRenderer>();
            Debug.Assert(_spriteRenderer != null);

            _enemyMovement = GetComponent<EnemyMovement>();

            LifeHandler.OnDeath = OnDeath;
            LifeHandler.OnHpChange = OnHpChange;
        }

        public void Update()
        {
            if (_frozenTime > 0)
            {
                _frozenTime -= Time.deltaTime;
            }
            else
            {
                IsFrozen = false;
            }

            if (_damageBlinkTime > 0)
            {
                _damageBlinkTime -= Time.deltaTime;
                _spriteRenderer.color = new Color(1.0f, 0.2f, 0.2f, 1.0f);
            }
            else if (IsFrozen)
            {
                _spriteRenderer.color = Color.blue;
            }
            else if (_enemyMovement != null && _enemyMovement.SpeedModifier < 1.0)
            {
                _spriteRenderer.color = new Color(0.5f, 0.5f, 1.0f, 1.0f);
            }
            else
            {
                _spriteRenderer.color = Color.white;
            }
        }

        public void OnEnable()
        {
            // HACK
            // TODO investigate why sometimes enemies are not reset properly
            // E.g., sometimes IsFrozen is set to true here
            Reset();
        }

        public void OnDeath()
        {
            //Debug.Log("Enemy died");
            switch (Type)
            {
                case EnemyType.Blob:
                    Pools.Instance.Blobs.PutObject(gameObject);
                    break;
                case EnemyType.Shooter:
                    Pools.Instance.Shooters.PutObject(gameObject);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void OnHpChange(float oldHp, float newHp, bool isHeal)
        {
            if (isHeal || !gameObject.activeSelf)
                return;
            _damageBlinkTime = 0.1f;
            SoundUtil.PlayRandomSound(HitSounds, _enemyAudioSource);
        }

        public void Reset()
        {
            //Debug.Log($"Resetting: {Id}");
            _enemyAudioSource.Stop();
            IsFrozen = false;
            _frozenTime = 0.0f;
            _damageBlinkTime = 0.0f;

            _spriteRenderer = GetComponent<SpriteRenderer>();
            LifeHandler = GetComponent<LifeHandler>();

            _spriteRenderer.color = Color.white;
            LifeHandler.Reset();

            var resettables = GetComponents<IResettable>();
            foreach (IResettable resettable in resettables)
            {
                if (!ReferenceEquals(resettable, this))
                    resettable.Reset();
            }
        }

        public void Freeze()
        {
            IsFrozen = true;
            _frozenTime = 2.0f;
        }
    }
}
