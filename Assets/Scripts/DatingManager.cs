﻿using Scripts.Girls;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts
{
    public class DatingManager : MonoBehaviour
    {
        public Text GirlsText;
        public Image GirlsImage;
        public Button LeaveButton;
        public Button NextLevelButton;
        public Button PlayAgainButton;
        public Button QuitGameButton;
        public Button[] Answers;
        public Sprite[] GirlsSprites;
        public Color[] GirlsColors;

        private GirlDialog.DialogPart _currentDialogPart;
        private GirlDialog _dialog;
        private GirlType _girlType;

        public void InitDating(GirlDialog dialog, GirlType type)
        {
            _girlType = type;
            GirlsImage.sprite = GirlsSprites[(int) type];
            GetComponent<Image>().color = GirlsColors[(int) type];

            _dialog = dialog;
            _currentDialogPart = dialog.GetFirstDialogPart(GameManager.Instance.GirlManager.GetGirlState(type).Level);
            NextLevelButton.gameObject.SetActive(false);
            LeaveButton.gameObject.SetActive(true);
            PlayAgainButton.gameObject.SetActive(false);
            QuitGameButton.gameObject.SetActive(false);
            InitDialogScreen();
        }

        private void InitDialogScreen()
        {
            foreach (Button answer in Answers)
            {
                answer.gameObject.SetActive(false);
            }

            for (int i = 0; i < _currentDialogPart.Answers.Length; i++)
            {
                Answers[i].gameObject.SetActive(true);
                GirlDialog.DialogAnswer dialogAnswer = _currentDialogPart.Answers[i];
                Answers[i].GetComponentInChildren<Text>().text = dialogAnswer.Text;
            }

            GirlsText.text = _currentDialogPart.Text;

            if (_currentDialogPart.LevelIncrease)
            {
                LeaveButton.gameObject.SetActive(false);
                if (GameManager.Instance.GirlManager.GetGirlState(_girlType).Level == 2)
                {
                    PlayAgainButton.gameObject.SetActive(true);
                    QuitGameButton.gameObject.SetActive(true);
                }
                else
                {
                    NextLevelButton.gameObject.SetActive(true);
                }
            }
        }

        public void SelectAnswer(int answerNo)
        {
            _currentDialogPart = _dialog.GetNextDialogPart(_currentDialogPart.Answers[answerNo].Id);
            InitDialogScreen();
        }

        public void GoToNextLevel()
        {
            GameManager.Instance.FinishLevel(_girlType);
        }

        public void PlayAgain()
        {
            GameManager.Instance.RestartGame();
        }

        public void QuitGame()
        {
            GameManager.Instance.QuitGame();
        }
    }
}
