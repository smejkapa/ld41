﻿using UnityEngine;

namespace Scripts
{
    public class PlayerTouchTrigger : MonoBehaviour
    {
        public void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                collision.gameObject.GetComponent<CharacterController>().OnEnemyTouched(transform.parent.gameObject);
            }
        }

        public void OnTriggerStay2D(Collider2D collision)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                collision.gameObject.GetComponent<CharacterController>().OnEnemyTouched(transform.parent.gameObject);
            }
        }
    }
}
