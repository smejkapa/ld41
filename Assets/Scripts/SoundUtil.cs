﻿using System.Collections.Generic;
using UnityEngine;

namespace Scripts
{
    internal static class SoundUtil
    {
        internal static void PlayRandomSound(IReadOnlyList<AudioClip> sounds, AudioSource source)
        {
            AudioClip sound = sounds[Random.Range(0, sounds.Count)];
            source.clip = sound;
            source.Play();
        }
    }
}
