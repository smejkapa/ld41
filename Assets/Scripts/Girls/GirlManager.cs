﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Scripts.Girls
{
    public class GirlManager
    {
        public int TotalGirlsLevel
        {
            get { return _girlStates.Values.Sum(x => x.Level); }
        }

        private readonly Dictionary<GirlType, GirlState> _girlStates;

        public GirlManager()
        {
            _girlStates = new Dictionary<GirlType, GirlState>();

            foreach (GirlType girlType in Enum.GetValues(typeof(GirlType)))
            {
                _girlStates.Add(girlType, new GirlState());
            }
        }

        public GirlState GetGirlState(GirlType girlType)
        {
            return _girlStates[girlType];
        }

    }
}
