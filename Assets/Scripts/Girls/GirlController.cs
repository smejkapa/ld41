﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts.Girls
{
    public class GirlController : MonoBehaviour
    {
        public List<Sprite> Sprites;
        public GirlType Type { get; set; }

        private SpriteRenderer _spriteRenderer;

        public GirlDialog Dialog { get; private set; }

        public void Awake()
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
            Debug.Assert(_spriteRenderer != null);
        }

        public void SetupGirl(GirlType type)
        {
            Type = type;
            _spriteRenderer.sprite = Sprites[(int) Type];

            switch (Type)
            {
                case GirlType.Fire:
                    Dialog = new FireDialog();
                    break;
                case GirlType.Frost:
                    Dialog = new FrostDialog();
                    break;
                case GirlType.Earth:
                    Dialog = new EarthDialog();
                    break;
                case GirlType.Air:
                    Dialog = new AirDialog();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.CompareTag("Player"))
            {
                FindObjectOfType<UIManager>().StartDating(Dialog, Type);
            }
        }
    }
}
