﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrostDialog : GirlDialog
{
    protected override void InitializeDialogs()
    {
        //LEVEL 0
        AddDialogPart(new DialogPart("0_START",
            "What do you want?",
            new DialogAnswer("0_LOST", "I'm lost. Can you give me directions to your heart?"),
            new DialogAnswer("0_COLD", "You look cold. Aren't you cold?"),
            new DialogAnswer("0_WHO", "Who are you?")
            ), 0);

        AddDialogPart(new DialogPart("0_LOST",
        "I'm not someone to make fun of, demon.",
        new DialogAnswer("0_LEAVE", "I'm sorry, didn't mean to insult you..."),
        new DialogAnswer("0_WHO", "Who are you then?")
        ));

        AddDialogPart(new DialogPart("0_LEAVE",
        "I have no time to bother with someone like you. Just leave."
        ));

        AddDialogPart(new DialogPart("0_COLD",
        "Cold is not something that bothers me. It's part of me. There's ice in my veins, ice in my heart. Ice is under my command, that's how it was for years and that's how it should be.",
        new DialogAnswer("0_ICE", "Ice? Under your command? What do you mean?"),
        new DialogAnswer("0_WHO", "You're not human. What are you then?")
        ));

        AddDialogPart(new DialogPart("0_WHO",
        "An ancient being, daughter of cold, commander of ice.",
        new DialogAnswer("0_OLD", "Ancient? How old are you?"),
        new DialogAnswer("0_ICE", "Ice? Wow. Could you elaborate?")
        ));

        AddDialogPart(new DialogPart("0_OLD",
        "Old enough.",
        new DialogAnswer("0_YOUNG", "You look pretty young. Kind of... frozen, but still beautiful."),
        new DialogAnswer("0_LEAVE", "Yuck!")
        ));

        AddDialogPart(new DialogPart("0_YOUNG",
        "Thank you, demon. Frozen, you say... I must admit, I'm flattered.",
        new DialogAnswer("0_ICE", "So what's the relationship between you and the ice?")
        ));

        AddDialogPart(new DialogPart("0_ICE",
        "I can bend ice to my will. Freeze something with a bare thought. Call a blizzard, if I wanted to.",
        new DialogAnswer("0_TEACH", "Are you able to... pass your knowledge?")
        ));

        AddDialogPart(new DialogPart("0_TEACH",
        "Of course. I can command the ice to do anything, even to obey someone else.",
        new DialogAnswer("0_DONE", "<LEVEL UP> Would you mind teaching me more about ice?")
        ));

        AddDialogPart(new DialogPart("0_DONE",
        "Let me think... Hmm, I believe I can spare a few hours and teach you a thing or two. For the sake of the future of the ice. \n\n <Your projectiles now slow enemies>",
        true
        ));

        //LEVEL 2
        AddDialogPart(new DialogPart("1_START",
            "Ah, it's you. How do you like the ice?",
            new DialogAnswer("1_LIKE", "I enjoy it more than a demon should."),
            new DialogAnswer("1_LIKE", "It's wonderful!"),
            new DialogAnswer("1_OTHERS", "It's fine, but I've seen better powers.")
            ), 1);

        AddDialogPart(new DialogPart("1_LIKE",
        "Glad you like it. Maybe I can teach you more. To freeze things, for example.",
        new DialogAnswer("1_FREEZE", "<LEVEL UP> That would be great!"),
        new DialogAnswer("1_OTHERS", "I want to try something... not cold.")
        ));

        AddDialogPart(new DialogPart("1_OTHERS",
        "Oh, you're speaking about the others? Flame? Mother Nature? Windy? Ok, go on then, run to them... go. Leave me."
        ));

        AddDialogPart(new DialogPart("1_FREEZE",
        "Ok, let's just walk and talk for a while, I'll explain everything... \n\n <Your projectiles now have chance to freeze enemies>",
        true
        ));

        //LEVEL 3
        AddDialogPart(new DialogPart("2_START",
            "Hey... come here for a while. I've been thinking.",
            new DialogAnswer("2_EXPLAIN", "What's on your mind?"),
            new DialogAnswer("2_EXPLAIN", "I'm here. I'm listening.")
            ), 2);

        AddDialogPart(new DialogPart("2_EXPLAIN",
        "I've been alone for a long time. I have forgotten how nice it could be to talk with someone. Work with someone.",
        new DialogAnswer("2_CONTINUE", "I'm here for you."),
        new DialogAnswer("2_CONTINUE", "Go on..."),
        new DialogAnswer("2_STOP", "Hold on. Slow down. I don't like where this is going.")
        ));

        AddDialogPart(new DialogPart("2_CONTINUE",
        "It would be nice if we could run away somewhere far away... and I don't know, start a blizzard. Together. You and me, you know.",
        new DialogAnswer("2_LIKE", "I would like that very much."),
        new DialogAnswer("2_LIKE", "We can..."),
        new DialogAnswer("2_STOP", "Wait. I don't... I can't.")
        ));

        AddDialogPart(new DialogPart("2_LIKE",
        "Should we do it then? We can leave right away...",
        new DialogAnswer("2_DONE", "<END GAME> As long as I'm with you, I'm happy."),
        new DialogAnswer("2_STOP", "No. There are some things I need to take care of.")
        ));

        AddDialogPart(new DialogPart("2_STOP",
        "Oh... I'm sorry, just forget about it then."
        ));

        AddDialogPart(new DialogPart("2_DONE",
        "Let's go then! Just you and me... and eternal winter! \n\n <THE END>",
        true
        ));

    }
}
