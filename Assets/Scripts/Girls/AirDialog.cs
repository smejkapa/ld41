﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirDialog : GirlDialog
{
    protected override void InitializeDialogs()
    {
        //LEVEL 0
        AddDialogPart(new DialogPart("0_START",
            "And maybe we're all just slaves... Oh, hi there.",
            new DialogAnswer("0_EXPLAIN", "What was that about?"),
            new DialogAnswer("0_CONTINUE", "You don't look like a slave."),
            new DialogAnswer("0_CONTINUE", "I'm not a slave."),
            new DialogAnswer("0_COMPLIMENT", "I would gladly be your slave.")
            ), 0);

        AddDialogPart(new DialogPart("0_EXPLAIN",
            "Uh, nothing? Freedom maybe... no, it was nothing.",
            new DialogAnswer("0_CONTINUE", "We're not slaves."),
            new DialogAnswer("0_COMPLIMENT", "A smile would suit you better than worries."),
            new DialogAnswer("0_WHO", "Aren't you free?")
        ));

        AddDialogPart(new DialogPart("0_CONTINUE",
            "Is that so? No, no... Don't let them fool you, that's what they're trying to do.",
            new DialogAnswer("0_WHO", "Why are you worrying so much?"),
            new DialogAnswer("0_COMPLIMENT", "I bet they wouldn't fool you."),
            new DialogAnswer("0_CARE", "Sweetie, just talk to me, it's gonna be okay.")
        ));

        AddDialogPart(new DialogPart("0_COMPLIMENT",
            "You know nothing about me... but then again, who does? Who cares nowadays about the others?",
            new DialogAnswer("0_WHO", "Why won't you tell me something about yourself then?"),
            new DialogAnswer("0_CARE", "I do."),
            new DialogAnswer("0_AGREE", "You're right. I don't care.")
        ));

        AddDialogPart(new DialogPart("0_AGREE",
            "(Gasp) I'm afraid I am."
        ));

        AddDialogPart(new DialogPart("0_CARE",
            "Oh. I'm sorry. You're right. You look like a nice per-... demon.",
            new DialogAnswer("0_WHO", "So tell me something about yourself.")

        ));

        AddDialogPart(new DialogPart("0_WHO",
            "I've been growing up alongside the wind. I used to think that makes me special, gives me something valuable... gives me freedom. But I was a fool.",
            new DialogAnswer("0_ELABORATE", "What happened?"),
            new DialogAnswer("0_AIR", "How does growing up alonside the wind look like?"),
            new DialogAnswer("0_AGREE", "Yes, you are.")
        ));

        AddDialogPart(new DialogPart("0_ELABORATE",
            "It doesn't matter who you are, where you are or what does your life look like... you may still be living in an invisible cage and nobody would notice. Sometimes not even the prisoner himself.",
            new DialogAnswer("0_STUCK", "Are you describing your life?"),
            new DialogAnswer("0_AIR", "Don't you have wind by your side?")
        ));

        AddDialogPart(new DialogPart("0_AIR",
            "Wind obeys me. I used to fly in the sky. Run faster than anyone else. I thought I couldn't be captured.",
            new DialogAnswer("0_STUCK", "What happened?"),
            new DialogAnswer("0_TEACH", "I don't want to be captured either.")
        ));

        AddDialogPart(new DialogPart("0_STUCK",
            "I don't know. I feel stuck. Like nothing was real.",
            new DialogAnswer("0_TEACH", "Can you teach me the wind thing?"),
            new DialogAnswer("0_TEACH", "Show me how the wind works and I promise you'll feel better."),
            new DialogAnswer("0_AGREE", "Sorry, you're alone in this.")
        ));

        AddDialogPart(new DialogPart("0_TEACH",
            "I could show you how to talk to wind and use it to be faster, but do you think it would change anything?",
            new DialogAnswer("0_DONE", "<LEVEL UP> Definitely. I would be faster."),
            new DialogAnswer("0_AGREE", "No. You're right. It wouldn't.")
        ));

        AddDialogPart(new DialogPart("0_DONE",
            "Come with me then... and be ready to be blown away. \n\n <Your speed increased>",
            true
        ));



        //LEVEL 1
        AddDialogPart(new DialogPart("1_START",
            "Run... away... run... away... ",
            new DialogAnswer("1_CONTINUE", "Excuse me?"),
            new DialogAnswer("1_BACK", "Ok then, as you wish...")
            ), 1);

        AddDialogPart(new DialogPart("1_BACK",
        "No! Sorry, come back, I didn't see you..."
        ));

        AddDialogPart(new DialogPart("1_CONTINUE",
        "Oh, sorry, I didn't notice you.",
        new DialogAnswer("1_EXPLAIN", "So what were you thinking about?"),
        new DialogAnswer("1_EXPLAIN", "Is something bothering you?")
        ));

        AddDialogPart(new DialogPart("1_EXPLAIN",
        "Nothing new... I feel trapped.",
        new DialogAnswer("1_TEACH", "Try thinking about something else... you can teach me again."),
        new DialogAnswer("1_LEAVE", "I'm sorry. I'll come back later then.")
        ));

        AddDialogPart(new DialogPart("1_TEACH",
        "What would you like to learn? How to attack with the speed of the wind?",
        new DialogAnswer("1_DONE", "<LEVEL UP> Sounds good."),
        new DialogAnswer("1_LATER", "I'll tell you when I'm ready.")
        ));

        AddDialogPart(new DialogPart("1_LATER",
        "Please do."
        ));


        AddDialogPart(new DialogPart("1_DONE",
        "Great. I'm looking forward to another evening with you. \n\n <You now shoot faster>",
        true
        ));


        //LEVEL 2
        AddDialogPart(new DialogPart("2_START",
            "I can't I can't I can't... ",
            new DialogAnswer("2_RUN", "This is it. I'm taking you away."),
            new DialogAnswer("2_EXPLAIN", "What's the matter?")
            ), 2);

        AddDialogPart(new DialogPart("2_EXPLAIN",
        "I can't say here. I need to run away, I have to...",
        new DialogAnswer("2_RUN", "I won't let you go alone. I'm going with you."),
        new DialogAnswer("2_LEAVE", "You have to stay strong.")
        ));

        AddDialogPart(new DialogPart("2_RUN",
        "Would you do it? Would you run away with me? Right now?",
        new DialogAnswer("2_DONE", "<END GAME> Right now."),
        new DialogAnswer("2_LEAVE", "Maybe not exactly right now... later?")
        ));

        AddDialogPart(new DialogPart("2_LEAVE",
        "Don't worry, I am strong... I will be fine."
        ));

        AddDialogPart(new DialogPart("2_DONE",
        "Maybe we can finally find our freedom... or at least, live in this cage together. \n\n <THE END>",
        true
        ));

    }
}
