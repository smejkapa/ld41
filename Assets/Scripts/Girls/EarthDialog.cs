﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EarthDialog : GirlDialog
{
    protected override void InitializeDialogs()
    {
        //LEVEL 0
        AddDialogPart(new DialogPart("0_START",
            "Welcome.",
            new DialogAnswer("0_WHO", "Nice to meet you."),
            new DialogAnswer("0_WAY", "Do you know where did the pretty lady go?"),
            new DialogAnswer("0_WHO", "Hi. I'm a demon. Who are you?")
            ), 0);

        AddDialogPart(new DialogPart("0_WHO",
        "They call me Mother Nature, I'm mother of all living things. How may I help you?",
        new DialogAnswer("0_OPTIONS", "What could you help me with?"),
        new DialogAnswer("0_COMPLIMENT", "You look too young for a mother. Young... and beautiful.")
        ));

        AddDialogPart(new DialogPart("0_WAY",
        "I know a lot of things, dear boy. Knowledge is earned by hard work, so don't get discouraged by failure, all you need to do is to try harder.",
        new DialogAnswer("0_OPTIONS", "What should I do to be as skilled as you?"),
        new DialogAnswer("0_COMPLIMENT", "You look young, but you sound wise.")
        ));

        AddDialogPart(new DialogPart("0_OPTIONS",
        "I could teach you something about life, how to appreciate it, how to save it, how to fix what is broken, how to improve what's not... Are you interested?",
        new DialogAnswer("0_TEACH", "Yes, will you teach me?"),
        new DialogAnswer("0_COMPLIMENT", "I can appreciate life. I can appreciate you.")
        ));

        AddDialogPart(new DialogPart("0_COMPLIMENT",
        "Thank you, son. Have you met the other ladies?",
        new DialogAnswer("0_LADIES", "Yes."),
        new DialogAnswer("0_LADIES", "No."),
        new DialogAnswer("0_LADIES", "I don't care about other ladies.")
        ));

        AddDialogPart(new DialogPart("0_LADIES",
        "There is one that tamed fire, one who has ice at her command and one that dances with the wind. They could teach you a lot.",
        new DialogAnswer("0_TEACH", "Could you teach me?"),
        new DialogAnswer("0_LEAVE", "Maybe I should find them.")
        ));

        AddDialogPart(new DialogPart("0_LEAVE",
        "Go, young one. And don't forget to keep your eyes and heart open."
        ));

        AddDialogPart(new DialogPart("0_TEACH",
        "I'm willing to teach you, but it will require your patience and concentration. We could start by making your body stronger.",
        new DialogAnswer("0_DONE", "<LEVEL UP> I'm ready."),
        new DialogAnswer("0_LEAVE", "I'll return later.")
        ));

        AddDialogPart(new DialogPart("0_DONE",
        "Follow me, first lesson awaits us... \n\n <Your health increased>",
        true
        ));

        //LEVEL 1
        AddDialogPart(new DialogPart("1_START",
            "Ready for another lesson?",
            new DialogAnswer("1_LESSON", "Couldn't we just sit and enjoy each other's company?"),
            new DialogAnswer("1_LESSON", "I am."),
            new DialogAnswer("1_LEAVE", "Sorry, I'm busy right now.")
            ), 1);

        AddDialogPart(new DialogPart("1_LEAVE",
        "Do what you have to do. You know where to find me."
        ));

        AddDialogPart(new DialogPart("1_LESSON",
        "There's so much to learn and so little time... What would you do if you saw the most beautiful flower in the world?",
        new DialogAnswer("1_KILL", "I would bring it to you."),
        new DialogAnswer("1_KILL", "I would take it and show it's beauty to everyone."),
        new DialogAnswer("1_CHERISH", "I would sit down and appreciate it's beauty.")
        ));

        AddDialogPart(new DialogPart("1_KILL",
        "No, no, little one... How could you kill something so precious?"
        ));

        AddDialogPart(new DialogPart("1_CHERISH",
        "Good. Live and let live. Would you like to learn how to regenerate your body after a fight?",
        new DialogAnswer("1_DONE", "<LEVEL UP> Certainly."),
        new DialogAnswer("1_LEAVE", "Maybe later.")
        ));

        AddDialogPart(new DialogPart("1_DONE",
        "Come with me and I'll show you the secret... \n\n <You regenerate part of you life after every fight>",
        true
        ));

        //LEVEL 2
        AddDialogPart(new DialogPart("2_START",
            "I think I got used to you too much.",
            new DialogAnswer("2_CONTINUE", "Is it a bad thing?"),
            new DialogAnswer("2_CONTINUE", "Maybe I got used to you too."),
            new DialogAnswer("2_LEAVE", "Maybe I should leave now.")
            ), 2);

        AddDialogPart(new DialogPart("2_LEAVE",
        "Go then, go in peace."
        ));

        AddDialogPart(new DialogPart("2_CONTINUE",
        "I enjoy your company. We could create great things together.",
        new DialogAnswer("2_GOAL", "Like... kids?"),
        new DialogAnswer("2_GOAL", "I'm sure we could."),
        new DialogAnswer("2_LEAVE", "Maybe, but not now.")
        ));

        AddDialogPart(new DialogPart("2_GOAL",
        "Would you like to join me on my journey? Would you like to bring peace to the world, heal and improve it? Would you like to stay by my side?",
        new DialogAnswer("2_DONE", "<END GAME> I would love to."),
        new DialogAnswer("2_LEAVE", "I can't right now. I need to go.")
        ));

        AddDialogPart(new DialogPart("2_DONE",
        "Come then, the world needs us... I need you. \n\n <THE END>",
        true
        ));



    }
}
