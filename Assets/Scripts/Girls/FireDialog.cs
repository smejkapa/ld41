﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireDialog : GirlDialog
{
    protected override void InitializeDialogs()
    {
        //LEVEL 0
        AddDialogPart(new DialogPart("0_START",
            "Hi there.",
            new DialogAnswer("0_CUTE", "Do you believe in love at first sight or should I pass by again?"),
            new DialogAnswer("0_FLAME", "You're hot."),
            new DialogAnswer("0_DOING", "Hi. What is someone like you doing here?")
            ), 0);

        AddDialogPart(new DialogPart("0_CUTE",
        "You're cute. So what brings you here? To this underground dungeon?",
        new DialogAnswer("0_DEMON", "I'm a demon, I like to go down-... underground."),
        new DialogAnswer("0_DOING", "I'm just hanging out. What brings you here?")
        ));

        AddDialogPart(new DialogPart("0_DOING",
        "I like burning up things once in a while. Many targets here.",
        new DialogAnswer("0_FLAME", "Sounds like fun."),
        new DialogAnswer("0_DEMON", "Would you attack even a demon?")
        ));

        AddDialogPart(new DialogPart("0_DEMON",
        "A demon? Can you control fire?",
        new DialogAnswer("0_TEACH", "No... not yet.")
        ));

        AddDialogPart(new DialogPart("0_FLAME",
        "I just love fire. Flames are my family. Burning bodies, trees, everything on fire... Oh... don't tell Mother Nature, she hates my work.",
        new DialogAnswer("0_TEACH", "I would enjoy that too."),
        new DialogAnswer("0_LATER", "Burning bodies? Oh, I forgot, I'm late to an appointment...")
        ));

        AddDialogPart(new DialogPart("0_TEACH",
        "I can teach you if you want. Add some fire to your blood. Make you more dangerous.",
        new DialogAnswer("0_DONE", "<LEVEL UP> Sure thing."),
        new DialogAnswer("0_LATER", "Maybe some other time...")
        ));

        AddDialogPart(new DialogPart("0_LATER",
        "See you later then. Come back whenever you want to burn things up!"
        ));

        AddDialogPart(new DialogPart("0_DONE",
        "Great! Come then, come with me, I'll explain everything! \n\n <Your projectiles now do more damage>",
        true
        ));

        //LEVEL 1
        AddDialogPart(new DialogPart("1_START",
            "Hello again, Fireboy.",
            new DialogAnswer("1_ENJOY", "Hello, Flame."),
            new DialogAnswer("1_ENJOY", "I've been longing to see you again.")
            ), 1);

        AddDialogPart(new DialogPart("1_ENJOY",
        "How do you enjoy you new skill?",
        new DialogAnswer("1_LIKE", "Love it! Everything is more... lightened up."),
        new DialogAnswer("1_OTHERS", "I'm not sure if this is my style.")
        ));

        AddDialogPart(new DialogPart("1_OTHERS",
        "And what is your style? Freezing things? Being fast like a wind? Caring about living things? Don't be ridiculous. None of that can compete with my flame aura.",
        new DialogAnswer("1_AURA", "Flame aura?")
        ));

        AddDialogPart(new DialogPart("1_LIKE",
        "Who wouldn't like it? So, shall we continue? What should be the next step? Flame aura, maybe?",
        new DialogAnswer("1_DONE", "<LEVEL UP> Let's do it!"),
        new DialogAnswer("1_AURA", "What's that?"),
        new DialogAnswer("1_LEAVE", "Maybe later, Flame. Not feeling up to it right now.")
        ));

        AddDialogPart(new DialogPart("1_AURA",
        "I can give you flame aura so you'll hurt things that touch you and it will also prevent some damage you take.",
        new DialogAnswer("1_DONE", "<LEVEL UP> Sounds good, bring it on!"),
        new DialogAnswer("1_LEAVE", "I'm not sure.")
        ));

        AddDialogPart(new DialogPart("1_LEAVE",
        "Just go and think about it, no one can give you what I can."
        ));

        AddDialogPart(new DialogPart("1_DONE",
        "So come with me and I'll burn you up... eh, help you. I'll help you. Don't worry, come... \n\n <You take less damage and you damage everything you touch>",
        true
        ));

        //LEVEL 2
        AddDialogPart(new DialogPart("2_START",
            "You know what? Let's make our own hell.",
            new DialogAnswer("2_EXPLAIN", "What do you mean?"),
            new DialogAnswer("2_EXPLAIN", "Great idea!"),
            new DialogAnswer("2_EXPLAIN", "You're out of your mind.")
            ), 2);

        AddDialogPart(new DialogPart("2_EXPLAIN",
        "We can just find a place, burn it up... make it famous...",
        new DialogAnswer("2_CONTINUE", "Burn it...?"),
        new DialogAnswer("2_CONTINUE", "Sounds like a dream."),
        new DialogAnswer("2_CONTINUE", "Stop it!")
        ));

        AddDialogPart(new DialogPart("2_CONTINUE",
        "It would be great. Just the two of us. What do you think about this place? We can burn up this place. It IS underground...",
        new DialogAnswer("2_END", "<END GAME> YEAAAH! BURN IT!"),
        new DialogAnswer("2_LEAVE", "Nope, you're crazy, I'm leaving...")
        ));

        AddDialogPart(new DialogPart("2_LEAVE",
        "Where do you think you're going? Come back!"
        ));

        AddDialogPart(new DialogPart("2_END",
        "Burn it! Burn it aaall! \n\n <THE END>",
        true
        ));

    }
}
