﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GirlDialog {
    protected GirlDialog()
    {
        InitializeDialogs();
    }

    public class DialogAnswer
    {
        public string Id { get; }
        public string Text { get; }

        public DialogAnswer(string id, string text)
        {
            Id = id;
            Text = text;
        }
    }

    public class DialogPart
    {
        public string Id { get; }
        public string Text { get; }
        public DialogAnswer[] Answers { get; }
        public bool LevelIncrease { get; }

        public DialogPart(string id, string text, params DialogAnswer[] answers) : this(id, text, false, answers)
        {
        }

        public DialogPart(string id, string text, bool levelIncrease, params DialogAnswer[] answers)
        {
            Id = id;
            Text = text;
            Answers = answers;
            LevelIncrease = levelIncrease;
        }
    }

    private readonly Dictionary<string, DialogPart> _dialogParts = new Dictionary<string, DialogPart>();

    private readonly Dictionary<int, string> _levelToDialogId = new Dictionary<int, string>();

    public DialogPart GetFirstDialogPart(int level)
    {
        string dialogId = _levelToDialogId[level];
        return _dialogParts[dialogId];
    }

    public DialogPart GetNextDialogPart(string dialogId)
    {
        return _dialogParts[dialogId];
    }

    protected abstract void InitializeDialogs();

    protected void AddDialogPart(DialogPart dialogPart, int? startingDialogForLevel = null)
    {
        _dialogParts.Add(dialogPart.Id, dialogPart);

        if (startingDialogForLevel.HasValue)
        {
            _levelToDialogId[startingDialogForLevel.Value] = dialogPart.Id;
        }
    }


    
}
