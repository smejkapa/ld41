﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Scripts
{
    public enum Direction
    {
        Up,
        Down,
        Left,
        Right
    }

    public static class Directions
    {
        public static readonly Dictionary<Direction, Vector2Int> VALUES = new Dictionary<Direction, Vector2Int>
        {
            { Direction.Up, new Vector2Int(0, 1) },
            { Direction.Down, new Vector2Int(0, -1) },
            { Direction.Left, new Vector2Int(-1, 0) },
            { Direction.Right, new Vector2Int(1, 0) }
        };

        public static Vector2Int ToVector(Direction direction)
        {
            return VALUES[direction];
        }

        public static Direction Opposite(Direction direction)
        {
            switch(direction)
            {
                case Direction.Up: return Direction.Down;
                case Direction.Down: return Direction.Up;
                case Direction.Left: return Direction.Right;
                case Direction.Right: return Direction.Left;
                default: throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
            }
        }
    }
}
