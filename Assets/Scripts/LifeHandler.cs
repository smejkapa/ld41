﻿using UnityEngine;

namespace Scripts
{
    public class LifeHandler : MonoBehaviour
    {
        public delegate void DeathCallback();
        public delegate void HpChangeCallback(float oldHp, float newHp, bool isHeal);

        public float MaxHP;
        public DeathCallback OnDeath;
        public HpChangeCallback OnHpChange;

        public bool IsInvulnurable { get; set; }

        public float CurrentHp { get; private set; }

        public void Start()
        {
            Reset();
        }

        public void Reset()
        {
            CurrentHp = MaxHP;
            OnHpChange?.Invoke(0, CurrentHp, true);
        }

        public void TakeDamage(float damage)
        {
            if (IsInvulnurable)
                return;

            var prevHp = CurrentHp;
            CurrentHp -= damage;

            if (CurrentHp <= 0.0f)
            {
                OnDeath();
            }

            if (CurrentHp < 0)
                CurrentHp = 0;

            OnHpChange?.Invoke(prevHp, CurrentHp, false);
            //Debug.Log("Damage taken");
            //Debug.Log($"Current HP: {CurrentHp}");
        }

        public void Heal(float healAmount)
        {
            var prevHp = CurrentHp;
            CurrentHp += healAmount;
            if (CurrentHp > MaxHP)
                CurrentHp = MaxHP;

            OnHpChange?.Invoke(prevHp, CurrentHp, true);
        }
    }
}
