﻿using System;
using System.Collections.Generic;
using System.Linq;
using Scripts.Girls;
using Scripts.Map;
using UnityEngine;
using UnityEngine.UI;

namespace Scripts
{
    public class UIManager : MonoBehaviour
    {
        public GameObject DatingPanel;
        public List<Text> GirlsLevels;
        public GridLayoutGroup Minimap;
        public Sprite NotVisitedSprite;
        public Sprite EmptySprite;
        public GameObject GameOverPanel;

        private List<Image> _minimapImages;

        public void Awake()
        {
            _minimapImages = Minimap.gameObject.GetComponentsInChildren<Image>().Where(x => x.gameObject != Minimap.gameObject).ToList();
        }

        public void Start()
        {
            UpdateGirlsLevels();
            DatingPanel.gameObject.SetActive(false);
            GameOverPanel.SetActive(false);
        }

        public void UpdateMinimap(Vector2Int currentRoom, Map.Map map)
        {
            int i = 0;
            for (int y = -1; y <= 1; ++y)
            {
                for (int x = -1; x <= 1; ++x)
                {
                    var dir = new Vector2Int(x, y);
                    Vector2Int roomPos = currentRoom + dir;
                    Room room;
                    if (!map.Rooms.TryGetValue(roomPos, out room))
                    {
                        //_minimapImages[i].sprite = NotVisitedSprite;
                        _minimapImages[i].sprite = null;
                        //_minimapImages[i].color = new Color(1, 1, 1, 0.5f);
                        _minimapImages[i].color = Color.black;
                    }
                    else
                    {
                        // Handle color
                        if (room.Position != currentRoom)
                        {
                            _minimapImages[i].color = new Color(1, 1, 1, 0.5f);
                        }
                        else
                        {
                            _minimapImages[i].color = Color.white;
                        }

                        // Handle images
                        if (room.IsCleared || room.Position == currentRoom)
                        {
                            if (room.Girl.HasValue)
                            {
                                _minimapImages[i].sprite = DatingPanel.GetComponent<DatingManager>()
                                    .GirlsSprites[(int) room.Girl.Value];
                            }
                            else
                            {
                                _minimapImages[i].color = new Color(1, 1, 1, 0);
                                _minimapImages[i].sprite = null;
                            }
                        }
                        else
                        {
                            _minimapImages[i].sprite = NotVisitedSprite;
                        }
                    }
                    i++;
                }
            }
        }

        private void UpdateGirlsLevels()
        {
            foreach (var type in Enum.GetValues(typeof(GirlType)))
            {
                //GirlsLevels[(int)type].text = $"{type} {GameManager.Instance.GirlManager.GetGirlState((GirlType)type).Level}/3";
                GirlsLevels[(int)type].text = $" {GameManager.Instance.GirlManager.GetGirlState((GirlType)type).Level}/3";
                //Debug.Log($"Girl {type} level: {GameManager.Instance.GirlManager.GetGirlState((GirlType)type).Level}");
            }
        }

        public void StartDating(GirlDialog dialog, GirlType type)
        {
            GameManager.Instance.IsInputEnabled = false;
            DatingPanel.SetActive(true);
            var datingManager = GetComponentInChildren<DatingManager>();

            GameManager.Instance.StartDating();
            datingManager.InitDating(dialog, type);
        }

        public void LeaveDating()
        {
            GameManager.Instance.StopDating();
            GameManager.Instance.IsInputEnabled = true;
            DatingPanel.SetActive(false);
        }

        public void QuitGame()
        {
            GameManager.Instance.QuitGame();
        }

        public void RestartGame()
        {
            GameManager.Instance.RestartGame();
        }

        public void GameOver()
        {
            GameOverPanel.SetActive(true);
        }
    }
}
