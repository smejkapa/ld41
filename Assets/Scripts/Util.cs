﻿using UnityEngine;

namespace Scripts
{
    public static class Util
    {
        private static readonly int PLAYER_LAYER = 8;
        private static readonly int ENEMY_LAYER = 9;
        private static readonly int PROJECTILE_LAYER = 10;

        public static void IgnoreEnemyPlayerCollisions(bool ignore)
        {
            Physics2D.IgnoreLayerCollision(PLAYER_LAYER, ENEMY_LAYER, ignore);
        }

        public static void IgnoreEnemyCollisions(bool ignore)
        {
            Physics2D.IgnoreLayerCollision(ENEMY_LAYER, ENEMY_LAYER, ignore);
        }

        public static void IgnoreProjectileCollisions(bool ignore)
        {
            Physics2D.IgnoreLayerCollision(PROJECTILE_LAYER, PROJECTILE_LAYER, ignore);
        }
    }
}
