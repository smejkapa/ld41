﻿using System;
using System.Collections.Generic;
using Scripts.Girls;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Scripts
{
    public class GameManager : MonoBehaviour
    {
        public AudioSource RoguelikeMusic;
        public AudioSource DatingMusic; 

        public static GameManager Instance { get; private set; }

        public GameObject Player { get; private set; }

        public GirlManager GirlManager { get; private set; }

        public bool IsInputEnabled { get; set; }

        private readonly List<ModUtil.CharacterModifier> _activeModifiers = new List<ModUtil.CharacterModifier>();

        void Awake()
        {
            if (Instance != null)
            {
                Destroy(gameObject);
                return;
            }

            Instance = this;
            DontDestroyOnLoad(gameObject);

            //Util.IgnoreEnemyCollisions(true);
            Util.IgnoreProjectileCollisions(true);

            Player = GameObject.FindWithTag("Player");

            GirlManager = new GirlManager();
            IsInputEnabled = true;

            RoguelikeMusic?.Play();

            DatingMusic?.Play();
            DatingMusic?.Pause();
        }

        public void OnLevelWasLoaded(int level)
        {
            Debug.Log("=== NEXT LEVEL ===");
            Player = GameObject.FindWithTag("Player");
            IsInputEnabled = true;
            foreach (ModUtil.CharacterModifier characterModifier in _activeModifiers)
            {
                characterModifier(Player);
            }

            RoguelikeMusic?.Stop();
            RoguelikeMusic?.Play();

            DatingMusic?.Stop();
            DatingMusic?.Play();
            DatingMusic?.Pause();
        }

        public void FinishLevel(GirlType type)
        {
            var girlLevel = ++GirlManager.GetGirlState(type).Level;

            if (girlLevel == 3)
            {
                Debug.Log("ERROR girl level too high!");
            }
            else
            {
                _activeModifiers.Add(ModUtil.MODIFIERS[type][girlLevel - 1]);
                SceneManager.LoadScene("MovementTest");
            }
        }

        public void GameOver()
        {
            Time.timeScale = 0.0f;
            IsInputEnabled = false;
            FindObjectOfType<UIManager>().GameOver();
        }

        public void RestartGame()
        {
            Time.timeScale = 1.0f;
            IsInputEnabled = true;
            _activeModifiers.Clear();
            GirlManager = new GirlManager();
            SceneManager.LoadScene("MovementTest");
        }

        public void QuitGame()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }

        public void StartDating()
        {
            RoguelikeMusic?.Pause();
            DatingMusic?.UnPause();
        }

        public void StopDating()
        {
            DatingMusic?.Pause();
            RoguelikeMusic?.UnPause();
        }
    }
}
