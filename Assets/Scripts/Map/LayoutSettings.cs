﻿


namespace Scripts.Map
{
    public class LayoutSettings
    {
        public int MaxRoomsForX { get; }
        public int MaxRoomsForY { get; }
        public int MinTotalRooms { get; }
        public int MaxTotalRooms { get; }
        public double DefaultRoomProbability { get; }
        public double ReturnTolerance { get; }
        public double MaxUnusedBonus { get; }

        public LayoutSettings()
        {
            //TODO 
            MaxRoomsForX = 10;
            MaxRoomsForY = 10;
            MinTotalRooms = 10;
            MaxTotalRooms = 20;
            DefaultRoomProbability = 0.5;
            ReturnTolerance = 1.5;
            MaxUnusedBonus = 0.2;
        }

        public LayoutSettings(int maxRoomsForX, int maxRoomsForY, int minTotalRooms, int maxTotalRooms, double defaultRoomProbability, double returnTolerance, double maxUnusedBonus)
        {
            MaxRoomsForX = maxRoomsForX;
            MaxRoomsForY = maxRoomsForY;
            MinTotalRooms = minTotalRooms;
            MaxTotalRooms = maxTotalRooms;
            DefaultRoomProbability = defaultRoomProbability;
            ReturnTolerance = returnTolerance;
            MaxUnusedBonus = maxUnusedBonus;
        }

    }
}
