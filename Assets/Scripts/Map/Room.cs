﻿using System.Collections.Generic;
using Scripts.Girls;
using UnityEngine;

namespace Scripts.Map
{
    public enum RoomType
    {
        Unknown,
        Start,
        Regular,
        Boss,
        Special
    }

    public enum EnemyType
    {
        Blob,
        Shooter
    }

    public class Room
    {
        public Vector2Int Position { get; set; }
        public RoomType Type { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public Dictionary<Direction, Door> Doors { get; set; }
        public bool IsCleared { get; set; }
        public GirlType? Girl { get; set; } = null;

        public Dictionary<Vector2Int, EnemyType> Enemies { get; } = new Dictionary<Vector2Int, EnemyType>();

        public Dictionary<Vector2Int, GameObject> WallGameObjects { get; } = new Dictionary<Vector2Int, GameObject>();
        public Dictionary<Vector2Int, GameObject> DoorsGameObjects { get; } = new Dictionary<Vector2Int, GameObject>();

        public Room(Vector2Int position)
        {
            Position = position;
            Type = RoomType.Unknown;
            Doors = new Dictionary<Direction, Door>();
        }

        public void CloseDoors()
        {
            foreach (GameObject door in DoorsGameObjects.Values)
            {
                door.GetComponent<DoorHandler>().CloseDoor();
            }
        }

        public void OpenDoors()
        {
            foreach (GameObject door in DoorsGameObjects.Values)
            {
                door.GetComponent<DoorHandler>().OpenDoor();
            }
        }
    }
}
