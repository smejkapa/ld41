﻿using System;
using System.Collections.Generic;
using System.Linq;
using Scripts.Girls;
using UnityEngine;

namespace Scripts.Map
{
    public class MapGenerator
    {
        private LayoutSettings _layoutSettings;

        public MapGenerator(LayoutSettings layoutSettings)
        {
            _layoutSettings = layoutSettings;
        }

        public Map GenerateMap()
        {
            Map map = GenerateRoomLayout();
            FillRooms(map);

            return map;
        }

        private Map GenerateRoomLayout()
        {
            var rooms = new Dictionary<Vector2Int, Room>();

            var firstRoom = new Room(new Vector2Int(0, 0))
            {
                Type = RoomType.Start
            };
            GenerateRoom(firstRoom);
            rooms.Add(firstRoom.Position, firstRoom);

            GenerateNeighbourRooms(firstRoom, rooms, new MetaProbabilities());

            while (rooms.Count < _layoutSettings.MinTotalRooms)
            {
                List<Room> roomList = new List<Room>(rooms.Values);
                GenerateNeighbourRooms(roomList[UnityEngine.Random.Range(0, roomList.Count -1)], rooms, new MetaProbabilities());
            }

            return new Map(firstRoom, rooms);
        }

        private void GenerateNeighbourRooms(Room room, Dictionary<Vector2Int, Room> rooms, MetaProbabilities metaProbabilities)
        {
            Probabilities probabilities = metaProbabilities.ToProbabilities(_layoutSettings.DefaultRoomProbability, _layoutSettings.ReturnTolerance, _layoutSettings.MaxUnusedBonus);

            foreach (var directionProbability in probabilities)
            {
                if (rooms.Count < _layoutSettings.MaxTotalRooms && UnityEngine.Random.value < directionProbability.Probability)
                {
                    if (!rooms.ContainsKey(room.Position + Directions.ToVector(directionProbability.Direction)))
                    {
                        //room does not exist, create it
                        Room newRoom = new Room(room.Position + Directions.ToVector(directionProbability.Direction));
                        GenerateRoom(newRoom);
                        rooms.Add(newRoom.Position, newRoom);

                        if (rooms.Count < _layoutSettings.MaxTotalRooms)
                        {
                            GenerateNeighbourRooms(newRoom, rooms, metaProbabilities.CalculateNewMetaProbabilities(directionProbability.Direction));
                        }
                    }

                    // create doors
                    AddDoors(room, directionProbability.Direction, rooms);
                }
                
            }
        }

        private void AddDoors(Room room, Direction direction, Dictionary<Vector2Int, Room> rooms)
        {
            Room targetRoom = rooms[room.Position + Directions.ToVector(direction)];

            if (!room.Doors.ContainsKey(direction) || !targetRoom.Doors.ContainsKey(Directions.Opposite(direction)))
            {
                room.Doors[direction] = Door.CreateDoorInTheMiddle(direction, room.Width, room.Height);
                targetRoom.Doors[Directions.Opposite(direction)] = Door.CreateDoorInTheMiddle(Directions.Opposite(direction), targetRoom.Width, targetRoom.Height);
            }
        }

        private void GenerateRoom(Room room)
        {
            //TODO 
            room.Width = 10;
            room.Height = 10;
        }

        private class RoomWithDistance : IComparable<RoomWithDistance>
        {
            public Room Room;
            public int Distance;
            private float _random;

            public RoomWithDistance(Room room, int distance)
            {
                Room = room;
                Distance = distance;
                _random = UnityEngine.Random.value;
            }

            public int CompareTo(RoomWithDistance other)
            {
                if (Distance != other.Distance)
                {
                    return Distance - other.Distance;
                }

                return _random > other._random ? 1 : -1;
            }
        }

        private class GirlWithLevel : IComparable<GirlWithLevel>
        {
            public GirlType GirlType { get; }
            public int Level { get; }
            private float _random;

            public GirlWithLevel(GirlType girlType, int level)
            {
                GirlType = girlType;
                Level = level;
                _random = UnityEngine.Random.value;
            }

            public int CompareTo(GirlWithLevel other)
            {
                if (Level != other.Level)
                {
                    return Level - other.Level;
                }

                return _random > other._random ? 1 : -1;
            }
        }

        private void FillRooms(Map roomLayout)
        {
            //generate girls to rooms
            GenerateGirlsToRooms(roomLayout);

            int level = GameManager.Instance.GirlManager.TotalGirlsLevel;

            var blobMinCount = (level / 2) + 1;
            var blobMaxCount = level + 2;

            var shooterMinCount = level / 2;
            var shooterMaxCount = Mathf.Min(level, 5);

            //generate enemies to all other rooms
            foreach (var room in roomLayout.Rooms.Values)
            {
                if (room.Type != RoomType.Start && room.Type != RoomType.Special)
                {
                    var botLeft = new Vector2Int(3, 3);
                    var topRight = new Vector2Int(room.Width - 3, room.Height - 3);
                    var emptySpaces = GenerateEmptySpaces(botLeft, topRight);

                    var blobCount = UnityEngine.Random.Range(blobMinCount, blobMaxCount);
                    var shooterCount = UnityEngine.Random.Range(shooterMinCount, shooterMaxCount);

                    for (int i = 0; i < shooterCount; ++i)
                    {
                        if (emptySpaces.Count == 0)
                            break;
                        var pos = FindRandomPos(emptySpaces);
                        room.Enemies.Add(pos, EnemyType.Shooter);
                    }

                    for (int i = 0; i < blobCount; ++i)
                    {
                        if (emptySpaces.Count == 0)
                            break;
                        var pos = FindRandomPos(emptySpaces);
                        room.Enemies.Add(pos, EnemyType.Blob);
                    }
                }  
            }
        }

        private static List<Vector2Int> GenerateEmptySpaces(Vector2Int botLeft, Vector2Int topRight)
        {
            var values = new List<Vector2Int>();
            for (int x = botLeft.x; x <= topRight.x; x++)
            {
                for (int y = botLeft.x; y <= topRight.y; y++)
                {
                    values.Add(new Vector2Int(x, y));
                }
            }

            return values;
        }

        private static Vector2Int FindRandomPos(IList<Vector2Int> emptySpaces)
        {
            var idx = UnityEngine.Random.Range(0, emptySpaces.Count);
            Vector2Int pos = emptySpaces[idx];
            emptySpaces.RemoveAt(idx);

            return pos;
        }

        private void GenerateGirlsToRooms(Map roomLayout)
        {
            //count distances
            List<RoomWithDistance> roomWithDistances = new List<RoomWithDistance>();
            Queue<RoomWithDistance> roomQueue = new Queue<RoomWithDistance>();
            HashSet<Vector2Int> visitedRooms = new HashSet<Vector2Int>();

            Room startRoom = roomLayout.StartRoom;
            RoomWithDistance startRoomWithDistance = new RoomWithDistance(startRoom, 0);
            roomQueue.Enqueue(startRoomWithDistance);
            visitedRooms.Add(startRoom.Position);
            //don't add start room to roomWithDistances

            while (roomQueue.Count > 0)
            {
                RoomWithDistance roomToScan = roomQueue.Dequeue();
                int roomDistance = roomToScan.Distance;

                foreach (Direction direction in roomToScan.Room.Doors.Keys)
                {
                    Room nextRoom = roomLayout.Rooms[roomToScan.Room.Position + Directions.ToVector(direction)];

                    if (!visitedRooms.Contains(nextRoom.Position))
                    {
                        RoomWithDistance nextRoomWithDistance = new RoomWithDistance(nextRoom, roomDistance + 1);
                        roomWithDistances.Add(nextRoomWithDistance);
                        roomQueue.Enqueue(nextRoomWithDistance);
                        visitedRooms.Add(nextRoom.Position);
                    }

                }
            }

            //sort rooms, most distant room last
            roomWithDistances.Sort();


            List<GirlWithLevel> girlWithLevels = new List<GirlWithLevel>();

            foreach (GirlType girlType in Enum.GetValues(typeof(GirlType)))
            {
                GirlState girlState = GameManager.Instance.GirlManager.GetGirlState(girlType);

                girlWithLevels.Add(new GirlWithLevel(girlType, girlState.Level));
            }

            //sort girls, most leveled girl last
            girlWithLevels.Sort();

            //put most leveled girls in farest rooms

            List<RoomWithDistance> roomsForGirls = new List<RoomWithDistance>();

            //add farest room to the list
            if (roomWithDistances.Count > 0)
            {
                int lastIndex = roomWithDistances.Count - 1;
                RoomWithDistance roomWithDistance = roomWithDistances[lastIndex];
                roomWithDistances.RemoveAt(lastIndex);

                roomWithDistance.Room.Type = RoomType.Special;
                roomsForGirls.Add(roomWithDistance);
            }

            //add random rooms to the list
            for (int i = 0; i < girlWithLevels.Count - 1; i++)
            {
                int index = UnityEngine.Random.Range(0, roomWithDistances.Count - 1);

                if (roomWithDistances[index].Distance <= 1)
                {
                    //try again
                    index = UnityEngine.Random.Range(0, roomWithDistances.Count - 1);
                }

                RoomWithDistance roomWithDistance = roomWithDistances[index];
                roomWithDistances.RemoveAt(index);

                roomWithDistance.Room.Type = RoomType.Special;
                roomsForGirls.Add(roomWithDistance);
            }

            //sort rooms, most distant room last
            roomsForGirls.Sort();

            //add girls to rooms, most leveled girl in farest room
            for (int i = 0; i < girlWithLevels.Count; i++)
            {
                roomsForGirls[i].Room.Girl = girlWithLevels[i].GirlType;
            }
            
            
        }
    }
}
