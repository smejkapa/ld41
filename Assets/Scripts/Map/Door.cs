﻿using UnityEngine;

namespace Scripts.Map
{
    public class Door
    {
        public Vector2Int Position { get; }

        public static Door CreateDoorInTheMiddle(Direction direction, int XWallSize, int YWallSize)
        {
            switch (direction)
            {
                case Direction.Up: return new Door(new Vector2Int(XWallSize / 2, YWallSize - 1));
                case Direction.Down: return new Door(new Vector2Int(XWallSize / 2, 0));
                case Direction.Left: return new Door(new Vector2Int(0, YWallSize /2));
                case Direction.Right: return new Door(new Vector2Int(XWallSize - 1, YWallSize / 2));
            }

            throw new System.ArgumentOutOfRangeException();
        }

        public Door(Vector2Int position)
        {
            Position = position;
        }
    }
}
