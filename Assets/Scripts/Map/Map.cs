﻿using System.Collections.Generic;
using UnityEngine;

namespace Scripts.Map
{
    public class Map
    {
        public Map(Room startRoom, Dictionary<Vector2Int, Room> rooms)
        {
            StartRoom = startRoom;
            Rooms = rooms;
        }

        public Room StartRoom { get; }
        public Dictionary<Vector2Int, Room> Rooms { get; }

        
    }
}
