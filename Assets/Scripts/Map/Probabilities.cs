﻿using Scripts;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectionProbability<T>
{
    public Direction Direction { get; set; }
    public T Probability { get; set; }

    public DirectionProbability(Direction direction, T probability)
    {
        Direction = direction;
        Probability = probability;
    }
}

public class Probabilities
{
    protected Dictionary<Direction, double> _directionalProbability { get; } = new Dictionary<Direction, double>();

    public Probabilities() : this(0)
    {

    }

    public Probabilities(double probability)
    {
        foreach (Direction direction in Enum.GetValues(typeof(Direction)))
        {
            _directionalProbability.Add(direction, probability);
        }
    }

    private Probabilities(Dictionary<Direction, double> directionalProbability)
    {
        _directionalProbability = directionalProbability;
    }

    public static Probabilities Create(Dictionary<Direction, double> directionalProbability)
    {
        return new Probabilities(directionalProbability);
    }

    public double this[Direction key]
    {
        get
        {
            return _directionalProbability[key];
        }

        set
        {
            _directionalProbability[key] = value;
        }

    }

    public IEnumerator<DirectionProbability<double>> GetEnumerator()
    {
        List<Direction> directions = new List<Direction>
        {
            Direction.Up,
            Direction.Down,
            Direction.Left,
            Direction.Right
        };

        while (directions.Count > 0)
        {
            int index = UnityEngine.Random.Range(0, directions.Count - 1);
            Direction direction = directions[index];
            directions.RemoveAt(index);
            yield return new DirectionProbability<double>(direction, _directionalProbability[direction]);
        }
    }

    public void Normalize()
    {
        double sum = 0;
        foreach (double value in _directionalProbability.Values)
        {
            sum += value;
        }

        if (sum != 0)
        {
            foreach (Direction direction in _directionalProbability.Keys)
            {
                _directionalProbability[direction] = _directionalProbability[direction] / sum;
            }
        }
    }

    public Probabilities Clone()
    {
        Dictionary<Direction, double> directionalProbability = new Dictionary<Direction, double>();

        foreach (Direction direction in Enum.GetValues(typeof(Direction)))
        {
            directionalProbability.Add(direction, _directionalProbability[direction]);
        }

        return new Probabilities(directionalProbability);
    }
}

public class MetaProbabilities
{
    public int Turn { get; }
    Dictionary<Direction, int> _lastGeneratedInTurn;
    Dictionary<Direction, int> _counts;

    public MetaProbabilities()
    {
        Turn = 0;
        _lastGeneratedInTurn = new Dictionary<Direction, int>();
        _counts = new Dictionary<Direction, int>();

        foreach (Direction direction in Enum.GetValues(typeof(Direction)))
        {
            _lastGeneratedInTurn[direction] = 0;
            _counts[direction] = 0;
        }
    }

    private MetaProbabilities(int turn, Dictionary<Direction, int> lastGeneratedInTurn, Dictionary<Direction, int> counts)
    {
        Turn = turn;
        _lastGeneratedInTurn = lastGeneratedInTurn;
        _counts = counts;
    }

    public Probabilities ToProbabilities(double defaultProbability, double returnTolerance, double maxUnusedBonus)
    {
        if (Turn == 0)
        {
            return new Probabilities(defaultProbability);
        }

        Probabilities probabilities = new Probabilities();

        int totalUses = 0;
        foreach (Direction direction in Enum.GetValues(typeof(Direction)))
        {
            totalUses += _counts[direction];
        }

        foreach (Direction direction in Enum.GetValues(typeof(Direction)))
        {
            if (_lastGeneratedInTurn[Directions.Opposite(direction)] > 0)
            {
                int turnsNotUsedOpposite = Turn - _lastGeneratedInTurn[Directions.Opposite(direction)];
                double bonus = maxUnusedBonus * (totalUses - _counts[direction]) / totalUses;

                double probability = Math.Min(1, Math.Max(0, defaultProbability + bonus - 1 / Math.Pow(returnTolerance, turnsNotUsedOpposite)));
                probabilities[direction] = probability;
            }
            else
            {
                probabilities[direction] = defaultProbability;
            }
        }

        return probabilities;
    }

    public MetaProbabilities CalculateNewMetaProbabilities(Direction selectedDirection)
    {
        Dictionary<Direction, int> lastGeneratedInTurn = new Dictionary<Direction, int>();
        Dictionary<Direction, int> counts = new Dictionary<Direction, int>();

        foreach (Direction direction in Enum.GetValues(typeof(Direction)))
        {
            lastGeneratedInTurn.Add(direction, _lastGeneratedInTurn[direction]);
            counts.Add(direction, _counts[direction]);
        }

        lastGeneratedInTurn[selectedDirection] = Turn + 1;
        counts[selectedDirection]++;
        return new MetaProbabilities(Turn + 1, lastGeneratedInTurn, counts);
    }
}

