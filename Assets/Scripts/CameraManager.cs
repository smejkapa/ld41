﻿using UnityEngine;

namespace Scripts
{
    public class CameraManager : MonoBehaviour
    {
        private GameObject _player;

        void Awake()
        {
            _player = GameObject.FindWithTag("Player");
            Debug.Assert(_player != null);
        }

        void Update()
        {
            Camera.main.transform.position = new Vector3(
                _player.transform.position.x,
                _player.transform.position.y,
                Camera.main.transform.position.z
            );
        }
    }
}
