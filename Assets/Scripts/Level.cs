﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using Scripts.Enemy;
using Scripts.Girls;
using UnityEngine;
using Scripts.Map;

namespace Scripts
{
    public class Level : MonoBehaviour
    {
        public GameObject WallPrefab;
        public GameObject DoorPrefab;
        public GameObject GirlPrefab;
        public GameObject FloorPrefab;
        public GameObject Tutorial;
        public Dictionary<Vector2Int, GameObject> RoomGameObjects { get; } = new Dictionary<Vector2Int, GameObject>();

        private Map.Map _map;
        private GameObject _player;
        private float _tileSize;
        private Vector2Int _currentRoom = Vector2Int.zero;

        public void Awake()
        {
            _player = GameObject.FindWithTag("Player");
            _tileSize = WallPrefab.GetComponent<SpriteRenderer>().bounds.size.x;
        }

        void Start()
        {
            var mg = new MapGenerator(new LayoutSettings());
            _map = mg.GenerateMap();
            GenerateLevel(_map);

            _currentRoom = _map.StartRoom.Position;
            RoomGameObjects[_currentRoom].SetActive(true);
            Room firstRoom = _map.Rooms[_currentRoom];
            firstRoom.IsCleared = true;
            _player.transform.position = new Vector3(
                (firstRoom.Width - 1) / 2.0f * _tileSize,
                (firstRoom.Height - 1) / 2.0f * _tileSize
            );
            _player.transform.parent = RoomGameObjects[_currentRoom].transform;
            FindObjectOfType<UIManager>().UpdateMinimap(_currentRoom, _map);

            Tutorial.SetActive(GameManager.Instance.GirlManager.TotalGirlsLevel == 0);
        }

        public void Update()
        {
            var room = _map.Rooms[_currentRoom];
            if (!room.IsCleared)
            {
                var roomGo = RoomGameObjects[_currentRoom];
                var enemies = roomGo.GetComponentsInChildren<EnemyController>();
                if (enemies.Length == 0)
                {
                    room.IsCleared = true;
                    room.OpenDoors();
                    GameManager.Instance.Player.GetComponent<CharacterController>().OnRoomCleared();
                }
            }
        }

        private void GenerateLevel(Map.Map map)
        {
            var mapGo = new GameObject("Map");
            int roomNo = 0;
            foreach (var mapRoom in map.Rooms)
            {
                var roomGo = new GameObject($"Room_{roomNo++}");
                roomGo.transform.parent = mapGo.transform;
                Room room = mapRoom.Value;

                foreach (var doors in room.Doors)
                {
                    Vector2Int doorsPos = doors.Value.Position;
                    GameObject doorsGo = Instantiate(DoorPrefab, roomGo.transform);
                    doorsGo.transform.position = new Vector3(doorsPos.x * _tileSize, doorsPos.y * _tileSize);
                    doorsGo.transform.Rotate(GetDoorRoration(doors.Key));
                    var handler = doorsGo.GetComponent<DoorHandler>();
                    handler.Level = this;
                    handler.Direction = doors.Key;
                    room.DoorsGameObjects.Add(doorsPos, doorsGo);
                }

                for (int x = 0; x < room.Width; ++x)
                {
                    AddWall(room, roomGo, x, 0);
                    AddWall(room, roomGo, x, room.Height - 1);
                }

                for (int y = 1; y < room.Height - 1; ++y)
                {
                    AddWall(room, roomGo, 0, y);
                    AddWall(room, roomGo, room.Width - 1 , y);
                }

                for (int x = 0; x < room.Width; ++x)
                {
                    for (int y = 0; y < room.Height; ++y)
                    {
                        var floor = Instantiate(FloorPrefab);
                        floor.transform.parent = roomGo.transform;
                        floor.transform.position = new Vector3(x * _tileSize, y * _tileSize);
                    }
                }

                foreach (var enemy in room.Enemies)
                {
                    GameObject enemyGo;
                    switch (enemy.Value)
                    {
                        case EnemyType.Blob:
                            enemyGo = Pools.Instance.Blobs.GetObject();
                            break;
                        case EnemyType.Shooter:
                            enemyGo = Pools.Instance.Shooters.GetObject();
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                    enemyGo.transform.parent = roomGo.transform;
                    enemyGo.transform.position = new Vector3(enemy.Key.x * _tileSize, enemy.Key.y * _tileSize);
                }

                if (room.Girl.HasValue)
                {
                    GameObject girlGo = Instantiate(GirlPrefab);
                    var girl = girlGo.GetComponent<GirlController>();
                    girl.SetupGirl(room.Girl.Value);
                    girlGo.transform.parent = roomGo.transform;
                    girlGo.transform.position = new Vector3(room.Width / 2.0f * _tileSize, room.Height / 2.0f * _tileSize);
                }

                roomGo.SetActive(false);
                RoomGameObjects.Add(mapRoom.Key, roomGo);
            }
        }

        private void AddWall(Room room, GameObject roomGo, int x, int y)
        {
            var posVec = new Vector2Int(x, y);
            // Do not place walls where doors are
            if (room.DoorsGameObjects.ContainsKey(posVec))
                return;

            GameObject wall = Instantiate(WallPrefab, roomGo.transform);
            wall.transform.position = new Vector3(x * _tileSize, y * _tileSize);
            room.WallGameObjects.Add(posVec, wall);
        }

        public void TriggerDoor(Direction doorDir)
        {
            Vector2Int doorDirVector = Directions.VALUES[doorDir];

            LoadRoom(doorDirVector);
            MovePlayer(doorDir, doorDirVector);
        }

        private void LoadRoom(Vector2Int dirVector)
        {
            Vector2Int lastRoom = _currentRoom;
            _currentRoom += dirVector;

            _player.transform.parent = RoomGameObjects[_currentRoom].transform;
            RoomGameObjects[lastRoom].SetActive(false);
            RoomGameObjects[_currentRoom].SetActive(true);

            // Remove all projectiles from last room
            ProjectileController[] projectiles = RoomGameObjects[lastRoom].GetComponentsInChildren<ProjectileController>(true);
            foreach (ProjectileController projectileController in projectiles)
            {
                switch (projectileController.Type)
                {
                    case ProjectileType.Fireball:
                        Pools.Instance.Fireballs.PutObject(projectileController.gameObject);
                        break;
                    case ProjectileType.BloodShot:
                        Pools.Instance.BloodShots.PutObject(projectileController.gameObject);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            Room room = _map.Rooms[_currentRoom];
            if (!room.IsCleared)
                room.CloseDoors();

            FindObjectOfType<UIManager>().UpdateMinimap(_currentRoom, _map);
            Tutorial.SetActive(GameManager.Instance.GirlManager.TotalGirlsLevel == 0 && room.Position == _map.StartRoom.Position);
        }

        private void MovePlayer(Direction doorDir, Vector2Int doorDirVector)
        {
            Direction oppoDir = Directions.Opposite(doorDir);
            Vector2Int doorPos = _map.Rooms[_currentRoom].Doors[oppoDir].Position;
            Vector3 playerPos =
                new Vector3(doorPos.x * _tileSize, doorPos.y * +_tileSize)
                + new Vector3(doorDirVector.x * _tileSize, doorDirVector.y * _tileSize);

            _player.transform.position = playerPos;
        }

        private static Vector3 GetDoorRoration(Direction dir)
        {
            switch (dir)
            {
                case Direction.Up:
                    return new Vector3(0, 0, 0);
                case Direction.Down:
                    return new Vector3(0, 0, 180);
                case Direction.Left:
                    return new Vector3(0, 0, 90);
                case Direction.Right:
                    return new Vector3(0, 0, -90);
                default:
                    throw new ArgumentOutOfRangeException(nameof(dir), dir, null);
            }
        }
    }
}
