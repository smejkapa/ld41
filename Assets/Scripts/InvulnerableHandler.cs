﻿using System.Linq.Expressions;
using UnityEngine;

namespace Scripts
{
    public class InvulnerableHandler : MonoBehaviour
    {
        public float MinAlpha;
        public float AlphaSpeed;

        private SpriteRenderer _spriteRenderer;
        private LifeHandler _lifeHandler;
        private bool _isGoingDown;

        public void Awake()
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
            Debug.Assert(_spriteRenderer != null);

            _lifeHandler = GetComponent<LifeHandler>();
            Debug.Assert(_lifeHandler != null);
        }

        public void Update()
        {
            if (_lifeHandler.IsInvulnurable)
            {
                float speed = _isGoingDown ? AlphaSpeed : -AlphaSpeed;
                float alpha = _spriteRenderer.color.a;
                float amount = Time.deltaTime * speed;
                if (alpha - amount < MinAlpha)
                {
                    amount -= alpha - MinAlpha;
                    amount = -amount;
                    _isGoingDown = !_isGoingDown;
                }
                else if (alpha - amount > 1.0f)
                {
                    amount += 1.0f - alpha;
                    amount = -amount;
                    _isGoingDown = !_isGoingDown;
                }

                _spriteRenderer.color = new Color(
                    _spriteRenderer.color.r,
                    _spriteRenderer.color.g,
                    _spriteRenderer.color.b,
                    alpha - amount
                );
            }
            else
            {
                _spriteRenderer.color = new Color(
                    _spriteRenderer.color.r,
                    _spriteRenderer.color.g,
                    _spriteRenderer.color.b,
                    1.0f
                );
                _isGoingDown = true;
            }
        }
    }
}
