﻿using System.Collections.Concurrent;
using Scripts.DataStructures;
using UnityEngine;

namespace Scripts
{
    public class Pools : MonoBehaviour
    {
        public GameObject ProjectilePrefab;
        public int ProjectileCount;

        public GameObject BlobPrefab;
        public int BlobCount;

        public GameObject ShooterPrefab;
        public int ShooterCount;

        public GameObject BloodShotPrefab;
        public int BloodShotCount;

        public static Pools Instance { get; private set; }

        public ObjectPool Fireballs { get; private set; }
        public ObjectPool Blobs { get; private set; }
        public ObjectPool Shooters { get; private set; }
        public ObjectPool BloodShots { get; private set; }

        public void Awake()
        {
            if (Instance != null)
            {
                Destroy(gameObject);
                return;
            }

            Instance = this;
            DontDestroyOnLoad(gameObject);
            Fireballs = new ObjectPool(ProjectileCount, transform, ProjectilePrefab);
            Blobs = new ObjectPool(BlobCount, transform, BlobPrefab);
            Shooters = new ObjectPool(ShooterCount, transform, ShooterPrefab);
            BloodShots = new ObjectPool(BloodShotCount, transform, BloodShotPrefab);
        }
    }
}
