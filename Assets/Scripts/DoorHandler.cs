﻿using UnityEngine;

namespace Scripts
{
    public class DoorHandler : MonoBehaviour
    {
        public Sprite OpenDoorSprite;
        public Sprite ClosedDoorSprite;

        public Level Level { get; set; }
        public Direction Direction { get; set; }

        private Collider2D _doorTrigger;
        private SpriteRenderer _spriteRenderer;

        private void Awake()
        {
            _doorTrigger = GetComponent<Collider2D>();
            Debug.Assert(_doorTrigger != null);

            _spriteRenderer = GetComponent<SpriteRenderer>();
            Debug.Assert(_spriteRenderer != null);
        }

        public void CloseDoor()
        {
            _doorTrigger.isTrigger = false;
            _spriteRenderer.sprite = ClosedDoorSprite;
        }

        public void OpenDoor()
        {
            _doorTrigger.isTrigger = true;
            _spriteRenderer.sprite = OpenDoorSprite;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.transform.gameObject.tag == "Player")
            {
                Level.TriggerDoor(Direction);
            }
        }
    }
}
